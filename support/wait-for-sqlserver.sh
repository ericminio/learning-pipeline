#!/bin/bash

function current_dir {
    echo $( cd "$( dirname "$1" )" >/dev/null 2>&1 && pwd )    
}
function execute {
    docker exec database /opt/mssql-tools/bin/sqlcmd -m 1 -S localhost -U SA -P Forever21! -d exploration -Q "$1"
}

DIR=$(current_dir ${BASH_SOURCE[0]})

ready=0
while [ "$ready" != "1" ]
do
    echo "waiting for SqlServer";
    docker exec database /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Forever21! -Q "SELECT 'yes' as SERVER_IS_READY" > $DIR/init.output
    ready=`grep yes $DIR/init.output | wc -l`
    sleep 1;
done;
echo "SqlServer is ready";

docker exec database /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Forever21! -Q "CREATE DATABASE exploration"

ready=0
while [ "$ready" != "1" ]
do
    echo "waiting for database";
    execute "SELECT 'yes' as DATABASE_IS_READY" > $DIR/init.output
    ready=`grep yes $DIR/init.output | wc -l`
    sleep 1;
done;
echo "Database is ready";